﻿## ftpPSPPushPull

A simple PowerShell script that pushes or pulls a folder and its files over FTP. 

I made this because I wanted to synchronize my saved game data with other devices without performing the synchronizing manually with a file explorer like FileZilla.
