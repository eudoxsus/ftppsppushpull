<#
 .SYNOPSIS
 Pushes/pulls files to/from a remote location

 .DESCRIPTION
 Pushes/pulls files to/from a remote location. Uses a config file using pairs of: RemoteLocation -> LocalLocation.

 .PARAMETER RemoteIp    
  Specifies the IP address of the remote location

 .PARAMETER ConfigPath    
  Specifies the file path to the config file to use

 .PARAMETER Push    
 Present if the user wants to push files to the remote location

 .PARAMETER Pull    
 Present if the user wants to pull files from the remote location

  .EXAMPLE
  C:\PS> .\ftpPSPPushPull.ps1 -RemoteIp 192.168.0.2:5000 -ConfigPath configFtp.txt -Pull

#>

param($RemoteIp,$ConfigPath,[switch]$Push,[switch]$Pull)

if ($Push.IsPresent -and $Pull.IsPresent) {
    Write-Host 'Supply only Push OR Pull, not both. Aborting now.'
    return
}

if (!$Push.IsPresent -and !$Pull.IsPresent) {
    Write-Host 'Please supply Push or Pull, none supplied. Aborting now.'
    return
}

Import-Module PSFTP

#LocationMap: RemoteLocation -> LocalLocation

$configReader = [System.IO.File]::OpenText($ConfigPath)
$locationMap = @{}

# Make a hashmap out of the config file 
while ($null -ne ($readLine = $configReader.ReadLine())){
    $locations = $readLine.Split(" ")
    $locationMap[$locations[0]] = $locations[1]
}

#The switch does not have any credentials, use anonymous
$FTPUsername = 'anonymous'
$FTPPassword = 'password'
$FTPSecurePassword = ConvertTo-SecureString -String $FTPPassword -asPlainText -Force
$FTPCredential = New-Object System.Management.Automation.PSCredential($FTPUsername,$FTPSecurePassword)

Set-FTPConnection -Server $RemoteIp -Session SyncSession -ignoreCert -Credentials $FTPCredential
$Session = Get-FTPConnection -Session SyncSession 

foreach($remoteLocation in $locationMap.Keys){
    $localLocation = $locationMap[$remoteLocation] 

    #Do FTP stuff here

    $ftpPath = "ftp://" + $RemoteIp + "/" + $remoteLocation

    if ($Pull.IsPresent) {
        Get-FTPChildItem -path $ftpPath -Recurse -Session $Session | Get-FTPItem -LocalPath $localLocation -Overwrite -Session $Session -RecreateFolders -Verbose
    }
    else{
        $files = Get-ChildItem -Path $localLocation -Recurse -File #| Add-FTPItem -Overwrite -Path $ftpPath -Session $Session -Verbose
        foreach($file in $files){
            $fullFtpPath = $ftpPath + "/" + $file.Directory.Name
            Add-FTPItem -Overwrite -Path $fullFtpPath -Session $Session -Verbose -localPath $file.FullName
        }
        
    }
    
}